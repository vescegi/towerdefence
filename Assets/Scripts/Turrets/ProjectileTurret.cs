using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ProjectileTurret : Turret
{
    private ShootProjectile shootProjectile;
    private void Awake()
    {
        shootProjectile = GetComponent<ShootProjectile>(); 
    }

    protected override void Shoot(Transform muzzle, float damage, float bulletSpeed, Transform target, float bias)
    {
        shootProjectile.Shoot(muzzle, damage, bulletSpeed, tag, target, bias);
    }
}
