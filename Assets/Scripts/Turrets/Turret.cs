using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class Turret : MonoBehaviour
{
    [SerializeField] protected TurretStats stats;
    [SerializeField] private Transform muzzle;
    [SerializeField] private LayerMask EnemyMask;
    [SerializeField] private float bias;

    private Base attachedBase;
    private float time;
    private bool canShoot;


    private Buff currentBuffs;
    public void Initialize(Base attachedBase)
    {
        this.attachedBase = attachedBase;
    }

    private void Start()
    {
        time = 0f;
    }

    private void Update()
    {
        currentBuffs = attachedBase.GetCurrentBuffs();

        Transform closestEnemy = GetClosestEnemy();
        LookAtEnemy(closestEnemy);

        ShootCooldown();

        if(canShoot && closestEnemy != null)
        {
            //Debug.Log(closestEnemy);
            Shoot(muzzle, stats.damage * currentBuffs.damage, stats.bulletSpeed, closestEnemy, bias);
            canShoot = false;
        }
    }

    private Transform GetClosestEnemy()
    {
        Collider enemyCollider = null;
        Collider[] enemyHit = Physics.OverlapSphere(transform.position, stats.range * currentBuffs.range, EnemyMask);

        float lastDistance = Mathf.Infinity;

        foreach(Collider hitted in enemyHit)
        {
            float distance = Vector3.Distance(transform.position, hitted.transform.position);
            if (distance < lastDistance)
            {
                lastDistance = distance;
                enemyCollider = hitted;
            }
        }

        if(enemyCollider)
            return enemyCollider.transform;
        else 
            return null;
    }
    private void LookAtEnemy(Transform enemy)
    {
        if (enemy == null) return;

        Vector3 position = enemy.transform.position;

        position.y = transform.position.y;

        transform.LookAt(position);
    }

    #region Shooting
    private void ShootCooldown()
    {
        time += Time.deltaTime;
        if (canShoot) return;
        
        float fireRate = stats.fireRate / currentBuffs.fireRate;


        if(time >= fireRate)
        {
            time = 0f;
            canShoot = true;
        }
    }

    #endregion

    protected abstract void Shoot(Transform muzzle, float damage, float bulletSpeed, Transform target, float bias);

#if UNITY_EDITOR
    [SerializeField] private bool showGizmos;
    private void OnDrawGizmos()
    {
        if(!showGizmos) return;

        Gizmos.color = Color.red;

        Gizmos.DrawWireSphere(transform.position, stats.range);
        Gizmos.DrawLine(muzzle.position, muzzle.forward * 100);
    }
#endif
}
