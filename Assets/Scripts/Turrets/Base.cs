using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Base : MonoBehaviour
{
    [SerializeField] private float baseHeight;
    [SerializeField] private float blockHeight;
    [SerializeField] private BlockHolder blockHolder;
    [SerializeField] private int maxStackHeight;

    private float currentHeight;

    private Buff currentBuffs;
    private int numberBlockPlaced;

    private void Start()
    {
        currentHeight = baseHeight;

        currentBuffs = new Buff(1f, 1f, 1f);
        numberBlockPlaced = 0;
    }

    public void AddBlock(BlockType block)
    {
        if (numberBlockPlaced >= maxStackHeight) return;
        if (block == BlockType.None) return;

        bool isTurret = block == BlockType.Simple || block == BlockType.MachineGun || block == BlockType.Bomb;

        if (!isTurret && numberBlockPlaced == 0) return;


        Vector3 position = transform.position;
        position.y += currentHeight;
        GameObject blockPlaced = Instantiate(blockHolder.GetPrefab(block), position, transform.rotation, transform);

        if (isTurret)
        {
            Turret turret = blockPlaced.GetComponent<Turret>();
            turret.Initialize(this);
        }
        else
        {
            currentBuffs *= blockPlaced.GetComponent<BuffBlock>().GetBuff();
        }

        numberBlockPlaced++;
        currentHeight += blockHeight;
    }

    public Buff GetCurrentBuffs()
    {
        return currentBuffs;
    }
}
