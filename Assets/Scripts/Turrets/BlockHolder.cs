using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "BlockHolder", menuName = "ScriptableObjects/BlockHolder")]
public class BlockHolder : ScriptableObject
{
    [Header("Turrets")]
    [SerializeField] GameObject simpleTurret;
    [SerializeField] GameObject machineGunTurret;
    [SerializeField] GameObject bombTurret;

    [Header("Buffs")]
    [SerializeField] GameObject damageBuff;
    [SerializeField] GameObject fireRateBuff;
    [SerializeField] GameObject rangeBuff;

    public GameObject GetPrefab(BlockType type)
    {
        GameObject prefab = null;

        switch (type)
        {
            case BlockType.None:
                prefab = null;
                break;

            case BlockType.Simple:
                prefab = simpleTurret;
                break;

            case BlockType.MachineGun:
                prefab = machineGunTurret;  
                break;
            
            case BlockType.Bomb: 
                prefab = bombTurret;
                break;

            case BlockType.DamageBuff:
                prefab = damageBuff;
                break;

            case BlockType.FireRateBuff:
                prefab = fireRateBuff;
                break;

            case BlockType.RangeBuff:
                prefab = rangeBuff;
                break;
        }

        return prefab;
    }
}
