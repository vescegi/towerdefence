using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public struct Buff
{
    public float damage;
    public float fireRate;
    public float range;

    public Buff(float damage, float fireRate, float range)
    {
        this.damage = damage;
        this.fireRate = fireRate;
        this.range = range;
    }

    public static Buff operator *(Buff a, Buff b)
    {
        return new Buff(a.damage * b.damage, a.fireRate * b.fireRate, a.range * b.range);
    }
}
public class BuffBlock : MonoBehaviour
{
    [SerializeField] private Buff buffToApply = new Buff(1f, 1f, 1f);

    public Buff GetBuff()
    {
        return buffToApply;
    }
}
