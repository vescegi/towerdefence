using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class HealthBar : MonoBehaviour
{
    [SerializeField] Image healthBar;

    public void ChangeFill(float fillAmount)
    {
        healthBar.fillAmount = fillAmount;
    }
}
