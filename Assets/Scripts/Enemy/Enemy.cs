using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Enemy : MonoBehaviour, IDamageable
{
    [SerializeField] private float moveSpeed;
    [SerializeField] private float minimumDistance = 0.001f;
    [SerializeField] private float maxHealth;
    [SerializeField] HealthBar healthBar;
    [SerializeField] private float damage;

    [HideInInspector] public bool IsReleased;

    private float health;

    private Path pathToFollow;
    private EnemyPool pool;
    private int index;


    public void Initialize(Path pathToFollow, EnemyPool pool)
    {
        health = maxHealth;

        healthBar.gameObject.SetActive(false);

        this.pathToFollow = pathToFollow;
        this.pool = pool;
        index = 0;
    }


    private void Update()
    {
        if (index >= pathToFollow.Count) return;

        Vector3 target = pathToFollow[index];

        Vector3 position = Vector3.MoveTowards(transform.position, target, moveSpeed * Time.deltaTime);
        

        transform.position = position;
        transform.LookAt(target);

        if(Vector3.Distance(position, target) < minimumDistance)
        {
            index++;
        }
    }

    public void TakeDamage(float damage)
    {
        //Debug.Log("ouch");
        Debug.Log(damage);

        healthBar.gameObject.SetActive(true);
        health -= damage;
        healthBar.ChangeFill(health / maxHealth);


        if(health <= 0 && !IsReleased)
        {
            pool.ReleaseEnemy(this);
        }
    }

    private void OnTriggerEnter(Collider other)
    {
        if(other.CompareTag("Barrier"))
        {
            //Debug.Log("a fess e mammt");
            other.GetComponent<IDamageable>().TakeDamage(damage);
            //pool.ReleaseEnemy(this);
            StartCoroutine(Die());
        }
    }

    IEnumerator Die()
    {
        yield return new WaitForSeconds(5f);

        pool.ReleaseEnemy(this);
    }
}
