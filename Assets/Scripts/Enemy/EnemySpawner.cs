using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemySpawner : MonoBehaviour
{
    [SerializeField] private Path path;

    [SerializeField] private float minimumSpawnTime;
    [SerializeField] private float maximumSpawnTime;

    [SerializeField] private GameObject enemyPrefab;
    [SerializeField] private Transform enemyFolder;

    private EnemyPool enemyPool;
    private float time;
    private float nextTime;

    private void Awake()
    {
        enemyPool = new EnemyPool(enemyPrefab, enemyFolder, transform.position);
        ResetTime();
    }

    void Update()
    {
        time += Time.deltaTime;
        
        if(time >= nextTime)
        {
            Enemy spawned = enemyPool.GetEnemy(transform);

            spawned.Initialize(path, enemyPool);

            ResetTime();
        }
    }

    private void ResetTime()
    {
        time = 0f;
        nextTime = Random.Range(minimumSpawnTime, maximumSpawnTime);
    }
}
