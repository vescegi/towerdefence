using System.Collections;
using System.Collections.Generic;
using Unity.VisualScripting;
using UnityEngine;
using UnityEngine.Pool;

public class EnemyPool
{
    private ObjectPool<Enemy> pool;
    private GameObject enemyPrefab;
    private Transform enemyFolder;
    private Vector3 defaultPosition;
    public EnemyPool(GameObject enemyPrefab, Transform enemyFolder, Vector3 defaultPosition)
    {
        this.enemyPrefab = enemyPrefab;
        this.enemyFolder = enemyFolder;
        this.defaultPosition = defaultPosition;

        pool = new ObjectPool<Enemy>(createFunc: CreateEnemy, actionOnGet: OnGet, actionOnRelease: OnRelease);
    }

    public Enemy GetEnemy(Transform initial)
    {
        Enemy enemy = pool.Get();

        enemy.transform.SetPositionAndRotation(initial.position, initial.rotation);

        return enemy;
    }

    public void ReleaseEnemy(Enemy enemy)
    {
        pool.Release(enemy);
    }

    #region Pooling Implementation
    private Enemy CreateEnemy()
    {
        GameObject enemy = GameObject.Instantiate(enemyPrefab, defaultPosition, Quaternion.identity, enemyFolder);

        enemy.SetActive(false);

        return enemy.GetComponent<Enemy>();

    }

    private void OnGet(Enemy enemy)
    {
        enemy.IsReleased = false;
        enemy.gameObject.SetActive(true);
    }

    private void OnRelease(Enemy enemy)
    {
        enemy.IsReleased = true;
        Vector3 position = enemy.transform.position;
        position.y -= 1000f;
        enemy.transform.position = position;

        enemy.gameObject.SetActive(false);
    }
    #endregion
}
