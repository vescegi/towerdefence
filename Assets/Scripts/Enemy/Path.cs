using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

public class Path : MonoBehaviour
{
    private List<Vector3> pathPoints = new List<Vector3>();
    
    public Vector3 this[int index]
    {
        get => pathPoints[index];
    }   

    public int Count
    {
        get => this.pathPoints.Count;
    }

    private void Awake()
    {
        pathPoints = new List<Vector3>();

        foreach (Transform children in transform)
        {
            pathPoints.Add(children.position);
        }
    }

#if UNITY_EDITOR
    private void OnDrawGizmos()
    {
        if (pathPoints == null) return;
        if (pathPoints.Count == 0) return;
        
        Handles.color = Color.green;

        for (int i = 0; i < pathPoints.Count - 1; i++)
        {
            Handles.DrawDottedLine(pathPoints[i], pathPoints[i + 1], 2f);
        }
    }
#endif
}
