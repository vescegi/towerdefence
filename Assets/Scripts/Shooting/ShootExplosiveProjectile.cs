using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ShootExplosiveProjectile : ShootProjectile
{
    protected override void GetProjectile(Transform muzzle, in ProjectileStats stats)
    {
        projectileManager.GetExplosiveProjectile(muzzle, stats);
    }
}
