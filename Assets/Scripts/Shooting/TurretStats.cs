using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "Turret Stats", menuName = "ScriptableObjects/TurretStats")]
public class TurretStats : ScriptableObject
{
    public float damage;
    public float fireRate;
    public float bulletSpeed;
    public float range;
}
