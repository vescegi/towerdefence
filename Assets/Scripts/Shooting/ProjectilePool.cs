using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Pool;

public class ProjectilePool<T> where T : Projectile
{
    private GameObject projectilePrefab;
    private ObjectPool<T> pool;

    public ProjectilePool(GameObject projectilePrefab)
    {
        pool = new ObjectPool<T>(createFunc: CreateProjectile, actionOnGet: OnGetProjectile, actionOnRelease: OnReleaseProjectile);
        this.projectilePrefab = projectilePrefab;
    }

    public T Get()
    {
        return pool.Get();
    }

    public void Release(T projectile)
    {
        pool.Release(projectile);
    }


    #region Pooling 
    private T CreateProjectile()
    {
        GameObject projectile = GameObject.Instantiate(projectilePrefab);
        projectile.SetActive(false);

        return projectile.GetComponent<T>();
    }

    private void OnGetProjectile(T projectile)
    {
        projectile.IsReleased = false;
        projectile.gameObject.SetActive(true);
    }

    private void OnReleaseProjectile(T projectile)
    {
        Vector3 position = projectile.transform.position;
        position.y -= 1000f;
        projectile.transform.position = position;

        projectile.IsReleased = true;
        projectile.gameObject.SetActive(false);
    }
    #endregion
}
