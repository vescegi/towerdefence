using System.Collections;
using System.Collections.Concurrent;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Pool;

public class ProjectileManager : MonoBehaviour
{
    private static ProjectileManager istance;
    public static ProjectileManager Instance
    {
        get => istance;

        private set => istance = value;
    }

    private ProjectilePool<NormalProjectile> normalProjectilePool;
    private ProjectilePool<ExplosiveProjectile> explosiveProjectilePool;

    [SerializeField] GameObject normalProjectilePrefab;
    [SerializeField] GameObject explosiveProjectilePrefab;
    [SerializeField] GameObject projectileFolder;

    private void Awake()
    {
        istance = this;
 
        normalProjectilePool = new ProjectilePool<NormalProjectile>(normalProjectilePrefab);
        explosiveProjectilePool = new ProjectilePool<ExplosiveProjectile>(explosiveProjectilePrefab);
    }

    public NormalProjectile GetNormalProjectile(Transform muzzle, in ProjectileStats projectileStats)
    {
        NormalProjectile projectile = normalProjectilePool.Get();

        // Rotate to the muzzle
        projectile.transform.SetPositionAndRotation(muzzle.position, muzzle.rotation);
        projectile.transform.parent = projectileFolder.transform;

        projectile.Init(projectileStats);

        return projectile;
    }

    public void ReleaseNormalProjectile(NormalProjectile projectile)
    {
        normalProjectilePool.Release(projectile);
    }

    public ExplosiveProjectile GetExplosiveProjectile(Transform muzzle, in ProjectileStats projectileStats)
    {
        ExplosiveProjectile projectile = explosiveProjectilePool.Get();

        // Rotate to the muzzle
        projectile.transform.SetPositionAndRotation(muzzle.position, muzzle.rotation);
        projectile.transform.parent = projectileFolder.transform;

        projectile.Init(projectileStats);

        return projectile;
    }

    public void ReleaseExplosiveProjectile(ExplosiveProjectile explosiveProjectile)
    {
        explosiveProjectilePool.Release(explosiveProjectile);
    }
}

