using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class ShootProjectile : MonoBehaviour
{
    protected ProjectileManager projectileManager;

    private void Start()
    {
        projectileManager = ProjectileManager.Instance;
    }

    public void Shoot(Transform muzzle, float damage, float speed, string shooter, Transform target, float bias)
    {
        ProjectileStats stats = new ProjectileStats(gameObject, speed, damage, shooter, target, bias);

        GetProjectile(muzzle, stats);
        //Debug.Log(muzzle.position);
    }

    protected abstract void GetProjectile(Transform muzzle, in ProjectileStats stats);

}
