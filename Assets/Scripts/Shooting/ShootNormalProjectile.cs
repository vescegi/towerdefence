using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ShootNormalProjectile : ShootProjectile
{
    protected override void GetProjectile(Transform muzzle, in ProjectileStats stats)
    {
        projectileManager.GetNormalProjectile(muzzle, stats);
    }
}