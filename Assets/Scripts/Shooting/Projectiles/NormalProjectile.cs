using System.Collections;
using System.Collections.Generic;
using Unity.VisualScripting.Antlr3.Runtime.Misc;
using UnityEngine;

public class NormalProjectile : Projectile
{
    protected override void Damage(Collision collision, float damage)
    {
        IDamageable damageable = collision.collider.GetComponent<IDamageable>();
        damageable?.TakeDamage(damage);
    }

    protected override void GoToQueue()
    {
        projectileManager.ReleaseNormalProjectile(this);
    }
}
