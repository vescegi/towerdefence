using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ExplosiveProjectile : Projectile
{
    [SerializeField] float explosionRadius;
    [SerializeField] LayerMask enemyMask;
    protected override void Damage(Collision collision, float damage)
    {
        var colliders = Physics.OverlapSphere(transform.position, explosionRadius, enemyMask);

        Debug.Log(colliders.Length);

        foreach (var collided in colliders)
        {
            collided.GetComponent<IDamageable>()?.TakeDamage(damage);
        }
    }

    protected override void GoToQueue()
    {
        projectileManager.ReleaseExplosiveProjectile(this);
    }

    private void OnDrawGizmos()
    {
        Gizmos.color = Color.green;
        Gizmos.DrawWireSphere(transform.position, explosionRadius);
    }
}
