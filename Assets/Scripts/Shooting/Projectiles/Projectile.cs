﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public struct ProjectileStats
{
    public GameObject shooter;
    public float speed;
    public float damage;
    public string shooterTag;
    public Transform target;
    public float bias;

    public ProjectileStats(GameObject shooter, float speed, float damage, string shooterTag, Transform target, float bias)
    {
        this.shooter = shooter;
        this.speed = speed;
        this.damage = damage;
        this.shooterTag = shooterTag;
        this.target = target;
        this.bias = bias;
    }
}

public abstract class Projectile : MonoBehaviour
{

    private Rigidbody rb;

    protected ProjectileManager projectileManager;

    private ProjectileStats stats;

    public bool IsReleased;

    private bool followTarget;

    public void Init(in ProjectileStats projectileStats)
    {
        stats = projectileStats;
        followTarget = true;
        //shooting.Play();
    }

    private void Awake()
    {
        rb = GetComponent<Rigidbody>();
        projectileManager = ProjectileManager.Instance;
    }

    private void Update()
    {
        Vector3 position;

        /* 
         * Because enemies are pooled, when they return to the scene fast enough
         * the projectiles could follow them when they should instead go straight
         * 😂🔫 😂🔫 😂🔫 😂🔫 😂🔫 😂🔫 😂🔫 😂🔫 😂🔫 😂🔫 😂🔫 😂🔫
         */
        if (!stats.target.gameObject.activeSelf)
        {
            followTarget = false;
        }


        if (followTarget)
        {
            position = stats.target.position;
            position.y += stats.bias;

            transform.LookAt(position);
        }
        else
        {
            position = transform.position + transform.forward * stats.speed;
        }

         transform.position = Vector3.MoveTowards(transform.position, position, stats.speed * Time.deltaTime);
    }

    private void OnCollisionEnter(Collision collision)
    {
        //Debug.Log("Shot " + collision.gameObject.name);

        Collider collided = collision.collider;

        if (collided.CompareTag(stats.shooterTag)) return;

        Damage(collision, stats.damage);

        HandleQueue();
    }
    
    private void HandleQueue()
    {
        if (IsReleased) return;
        GoToQueue();
    }

    protected abstract void Damage(Collision collision, float damage);
    protected abstract void GoToQueue();
}