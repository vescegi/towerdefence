using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public enum BlockType
{
    None,
    Simple,
    MachineGun,
    Bomb,
    DamageBuff,
    FireRateBuff,
    RangeBuff
}

public class UIManager : MonoBehaviour
{
    private UIManager instance;
    public UIManager Instance { get => instance; set => instance = value; }

    private BlockType selectedBlock;

    public BlockType SelectedBlock { get => selectedBlock; }

    [SerializeField] LayerMask baseMask;

    private void Awake()
    {
        instance = this;
    }

    private void Update()
    {
        if(Input.GetMouseButtonDown(0))
        {
            //Debug.Log("Click");

            Ray cameraRay = Camera.main.ScreenPointToRay(Input.mousePosition);
            if(Physics.Raycast(cameraRay, out RaycastHit hitInfo,100f, baseMask))
            {
                //Debug.Log("Hit");
                hitInfo.transform.GetComponent<Base>().AddBlock(selectedBlock);
            } 
        }
    }

    public void ChangeBlockType(int type)
    {
        selectedBlock = (BlockType)type;

        Debug.Log("Selected " +  selectedBlock);
    }

    public void Restart()
    {
        SceneManager.LoadScene(SceneManager.GetActiveScene().name);
    }

    public void Quit()
    {
        Application.Quit();
    }
}
