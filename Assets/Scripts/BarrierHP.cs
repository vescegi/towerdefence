using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class BarrierHP : MonoBehaviour, IDamageable
{
    [SerializeField] float maxHP;

    private HealthBar healthBar;
    private float HP;    
    private void Awake()
    {
        healthBar = GetComponent<HealthBar>();
        HP = maxHP;
    }

    public void TakeDamage(float damage)
    {
        //Debug.Log("strunz");
        HP -= damage;

        if (HP <= 0)
        {
            HP = 0;
            SceneManager.LoadScene(SceneManager.GetActiveScene().name);
        }

        healthBar.ChangeFill(HP / maxHP);
    }
}
